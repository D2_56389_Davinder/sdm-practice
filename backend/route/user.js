const express = require('express');
const cors = require('cors');
const utils = require('../utils');
const db = require('../db');

const router = express.Router()

router.post("/",(request,response)=>
{
    const {name,age , email}=request.body
    const connection=db.openconection()
    const statement = `insert into user (name,age,email) values
                        ('${name}','${age}','${email}')`
    
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})


router.get("/",(request,response)=>
{
    
    const connection=db.openconection()
    const statement = `select uid ,name,age,email from user`
    
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})

router.put("/",(request,response)=>
{
    const {id,name,age , email}=request.body
    const connection=db.openconection()
    const statement = `update user set name='${name}',age=${age},email='${email}' where uid = ${id}`
    
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})

router.delete("/",(request,response)=>
{
    const {id}=request.body
    const connection=db.openconection()
    const statement = `delete from user where uid=${id}`
    
    connection.query(statement,(error,result)=>
    {
        response.send(utils.createResult(error,result))
    })
})
module.exports=router