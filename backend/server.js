const express = require('express');
const cors = require('cors');
const userRouter = require('./route/user')
const productRouter = require('./route/product');
const app=express()

app.use(express.json())
app.use(cors ("*"))
app.use("/user",userRouter)
app.use("/product",productRouter)

app.use("/",(request,response)=>
{
    response.send(`hello everyone`)
})

app.listen(4000,"0.0.0.0",()=>
{
    console.log(`server started on port 4000`)

})
